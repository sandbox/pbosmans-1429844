<?php

/**
 * @file
 * Rules integration with the vscp module.
 */

/**
* Implementation of hook_rules_event_info().
* @ingroup rules
*/
function vscp_rules_event_info() {
  return array(
    'vscp_event' => array(
      'label' => t('After a VSCP event is received'),
      'module' => 'Vscp',
      'arguments' => array(
        'none' => array('type' => 'node', 'label' => t('The corresponding node of the vscp event.')),
      ),
    ),
  );
}
/**
 * Implementation of hook_rules_action_info().
 */
function vscp_rules_action_info() {
  return array(
    'vscp_action_send_event' => array(
      'label' => t('Send VSCP event'),
      'arguments' => array(
        'nid' => array('type' => 'string', 'label' => t('The node-id to be send as a VSCP-event.')),
      ),
      'module' => 'Vscp',
    ),
  );
}
/**
 * implementation of hook_rules_condition_info()
 */
function vscp_rules_condition_info() {
  return array(
    'vscp_condition_event_class' => array(
      'label' => t('VSCP event has class'),
      'arguments' => array(
        'Node' => array('type' => 'node', 'label' => t('The vscp-node to check.')),
        'Level-I-Class' => array('type' => 'string', 'label' => t('The level I class is')),
      ),
      'module' => 'Vscp',
    ),
    'vscp_condition_event_type' => array(
      'label' => t('VSCP event has type'),
      'arguments' => array(
        'Node' => array('type' => 'node', 'label' => t('The vscp-node to check.')),
        'Level-I-Type' => array('type' => 'string', 'label' => t('The level I type is')),
      ),
      'module' => 'Vscp',
    ),
    'vscp_condition_event_zone' => array(
      'label' => t('VSCP event has zone'),
      'arguments' => array(
        'Node' => array('type' => 'node', 'label' => t('The vscp-node to check.')),
        'Zone' => array('type' => 'string', 'label' => t('The zone is')),
      ),
      'module' => 'Vscp',
    ),
    'vscp_condition_event_subzone' => array(
      'label' => t('VSCP event has subzone'),
      'arguments' => array(
        'Node' => array('type' => 'node', 'label' => t('The vscp-node to check.')),
        'Subzone' => array('type' => 'string', 'label' => t('The subzone is')),
      ),
      'module' => 'Vscp',
    ),
    'vscp_condition_event_byte0' => array(
      'label' => t('VSCP event has byte0'),
      'arguments' => array(
        'Node' => array('type' => 'node', 'label' => t('The vscp-node to check.')),
        'Level-I-Byte0' => array('type' => 'string', 'label' => t('The level I byte0 is')),
      ),
      'module' => 'Vscp',
    ),
    'vscp_condition_event_databytes' => array(
      'label' => t('VSCP event has databytes'),
      'arguments' => array(
        'Node' => array('type' => 'node', 'label' => t('The vscp-node to check.')),
        'Level-I-Databytes' => array('type' => 'string', 'label' => t('The level I databytes is')),
      ),
      'module' => 'Vscp',
    ),
  );
}

/**
 * Condition packages_condition_event_class
 */
function vscp_condition_event_class($node, $class) {
  $term_syns = _vscpfindsyns($node);
//  watchdog('vscp', t('VSCP event compare class !nodeclass <=> !class',array('!class' => $class, '!nodeclass' => $term_syns['class'])));
  return ($term_syns['class'] == $class) ? true : false;
}
/**
 * Condition packages_condition_event_type
 */
function vscp_condition_event_type($node, $type) {
  $term_syns = _vscpfindsyns($node);
//  watchdog('vscp', t('VSCP event compare type !nodetype <=> !type',array('!type' => $type, '!nodetype' => $term_syns['type'])));
  return ($term_syns['type'] == $type) ? true : false;
}
/**
 * Condition packages_condition_event_zone
 */
function vscp_condition_event_zone($node, $zone) {
  $term_syns = _vscpfindsyns($node);
//  watchdog('vscp', t('VSCP event compare zone !nodezone <=> !zone',array('!zone' => $zone, '!nodezone' => $term_syns['zone'])));
  return ($term_syns['zone'] == $zone) ? true : false;
}
/**
 * Condition packages_condition_event_subzone
 */
function vscp_condition_event_subzone($node, $subzone) {
  $term_syns = _vscpfindsyns($node);
//  watchdog('vscp', t('VSCP event compare subzone !nodesubzone <=> !subzone',array('!subzone' => $subzone, '!nodesubzone' => $term_syns['subzone'])));
  return ($term_syns['subzone'] == $subzone) ? true : false;
}
/**
 * Condition packages_condition_event_byte0
 */
function vscp_condition_event_byte0($node, $byte0) {
  $term_syns = _vscpfindsyns($node);
//  watchdog('vscp', t('VSCP event compare byte0 !nodebyte0 <=> !byte0',array('!byte0' => $byte0, '!nodebyte0' => $term_syns['byte0'])));
  return ($term_syns['byte0'] == $byte0) ? true : false;
}
/**
 * Condition packages_condition_event_databytes
 */
function vscp_condition_event_databytes($node, $databytes) {
//  watchdog('vscp', t('VSCP event compare data !nodedata <=> !data',array('!data' => $databytes, '!nodedata' => $node->field_data[0]['value'])));
    return ($node->field_data[0]['value'] == $databytes) ? true : false;
}

/**
 * Action send VSCP event
 */
function vscp_action_send_event($nid) {
  $node = node_load($nid);
  _vscpsendevent($node);
}

