<?php

$args = getopt("e:c:t:h:p:s:d:o:h:g:i:");
print_r($args);

#if (isset($args[e])) {
if (isset($args[c]) && isset($args[t]) && isset($args[s]) && isset($args[d])) {
  if ($args[s] >= 3) {
    $data = preg_split("/,/",$args[d]);
    $byte0 = hexdec($data[0]);
    $zone = hexdec($data[1]);
    $subzone = hexdec($data[2]);
    $eventdata = "";
    for ($i = 3; $args[s] > $i; $i++) {
      $eventdata .= hexdec($data[$i]);
    }
  }
  $arrArgs = array($args[c],$args[t],$zone,$subzone,$byte0,$eventdata);
  $ctxtSysConnect = assemble_request('vscp.event', $arrArgs, FALSE);
  $file = file_get_contents("http://vscp/xmlrpc.php", false, $ctxtSysConnect);
  $response = xmlrpc_decode($file);
  dump_response($response);
}

function assemble_request($method, $args = array(), $dump_request = FALSE){
        $request = xmlrpc_encode_request($method,$args);
        $context = stream_context_create(array('http' => array(
                'method' => "POST",
                'header' => "Content-Type: text/xml",
                'content' => $request
        )));
        if($dump_request){
                echo '<pre>';
                echo htmlentities($request);
                echo '</pre>';
        }
        return $context;
}

function dump_response($response){
       echo "Drupal response\n";
                if (xmlrpc_is_fault($response)) {
                        trigger_error("xmlrpc: $response[faultString] ($response[faultCode])");
                } else {
                        print_r($response);
                }
}

?>