<?php
// $Id:$ 

/**
 * @file 
 * Administration page callbacks for the vscp module.
 */

/**
 * Form builder. Configure VSCP
 *
 */
function vscp_admin_settings() {
  $form = array();
  
  $form['vscp_daemon_settings_host'] = array(
    '#type' => 'textfield',
    '#title' => t('VSCP daemon host'),
    '#description' => t('Sets VSCP daemon host address.'),
    '#default_value' => variable_get('vscp_daemon_settings_host', 'localhost'),
    '#size' => 60,
    '#maxlength' => 90,
  );
  $form['vscp_daemon_settings_port'] = array(
    '#type' => 'textfield',
    '#title' => t('VSCP daemon port'),
    '#description' => t('Sets VSCP daemon port number.'),
    '#default_value' => variable_get('vscp_daemon_settings_port', '9598'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  
  return system_settings_form($form);
}
